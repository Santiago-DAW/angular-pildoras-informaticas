import { Component } from '@angular/core';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent {
  nombre = 'Juan';
  apellido = 'Díaz';
  edad = 18;
  empresa = 'Google';

  isDisabled = true;
  usuarioRegistrado = false;
  textoDeRegistro = 'No hay nadie registrado';

  getRegistroUsuario() {
    this.usuarioRegistrado = false;
  }

  setUsuarioRegistrado() {
    // alert('El usuario se acaba de registrar');
    this.textoDeRegistro = 'El usuario se acaba de registrar';
  }

  setUsuarioRegistradoEvento(event: Event) {
    // alert(event.target);
    // Hacemos un casting.
    // Esto se hace para permitir el acceso a las propiedades 
    // específicas de un elemento de entrada HTML como .value 
    // que no están disponibles en el tipo genérico EventTarget.
    if((<HTMLInputElement>event.target).value == 'si') {
      this.textoDeRegistro = 'El usuario se acaba de registrar';
    } else {
      this.textoDeRegistro = 'No hay nadie registrado';
    }
  }

  cambiaEmpresa(event: Event) {
    this.empresa = (<HTMLInputElement>event.target).value;
  }
}
