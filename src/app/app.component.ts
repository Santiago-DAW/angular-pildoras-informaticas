import { Component } from '@angular/core';

// COMPONENTE PRINCIPAL
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pildoras-informaticas';
  saludo = 'Hola desde Angular';
}
