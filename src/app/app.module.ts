import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { EjemploInlineComponent } from './ejemplo-inline/ejemplo-inline.component';

// MÓDULO RAIZ
@NgModule({
  declarations: [
    AppComponent,
    EmpleadosComponent,
    EmpleadoComponent,
    EjemploInlineComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  // Define el componente principal.
  bootstrap: [AppComponent]
})
export class AppModule { }
