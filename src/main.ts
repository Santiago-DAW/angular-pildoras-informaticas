import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';


// MAIN
// Especifica cuál es el módulo raíz, el módulo que debe cargar 
// la aplicación.
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
